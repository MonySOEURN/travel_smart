<?php
// use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'User\GuestController@index');

Auth::routes();

// Route::group(['middleware' => 'auth'], function () {
//     Route::post('/students/{id}/edit', 'StudentController@edit');
//     Route::post('/students/{id}/delete', 'StudentController@destroy');
//     Route::post('/students/store', 'StudentController@store');
// });

// Route::get('/home', 'HomeController@index')->name('home');

// Route::get('/students/create', 'StudentController@create')->name('CreateStudent');
// Route::post('/students/store', 'StudentController@store')->name('NewStudentInfo');
// Route::get('/students/show', 'StudentController@show')->name('ShowaStudent');
// Route::get('/students/index', 'StudentController@index');
// Route::get('/students/{id}/edit', 'StudentController@edit')->name('EditStudent');
// Route::post('/students/update/{id}', 'StudentController@update')->name('UpdateStudent');
// Route::get('/students/{id}/delete', 'StudentController@destroy')->name('DeleteStudent');

// route for admin
Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin',
    'middleware' => ['auth','admin_access']
], function () {
    Route::get('/dashboard','DashboardController@index')->name('AdminDashboard');
    // placecontrol in admin
    Route::get('/place','PlaceController@index')->name('AdminShowAllPlace');
    Route::get('/place/create','PlaceController@createPlace')->name('AdminCreatePlace');
    Route::post('/place/store','PlaceController@storePlace')->name('AdminStorePlace');
    Route::get('/place/{id}/edit','PlaceController@editPlace')->name('AdminEditPlace');
    Route::post('/place/{id}/update','PlaceController@updatePlace')->name('AdminUpdatePlace');
    Route::get('/place/{id}/delete','PlaceController@deletePlace')->name('AdminDeletePlace');
    // placecategory in admin
    Route::get('/category','CategoryController@index')->name('AdminShowAllCategory');


});
// Route::get('/admin','Admin\PlaceController@createPlace')->name('CreatePlace');

Route::group([
    'prefix' => 'myguest',
    'namespace' => 'User',
], function () {

    // route for tourist user
    Route::get('/','GuestController@index')->name('UserHome');
    Route::get('/places','GuestController@all_category_place')->name('UserAllCategoryPlace');
    Route::get('/saved_place','GuestController@all_category_place')->name('UserShowSavePlace');
    Route::get('/plan','GuestController@all_category_place')->name('UserShowPlan');
    Route::get('/{id}/list','GuestController@list_all_places')->name('UserlistAllPlacesInCategory');

    // route for user to save place
    Route::post('/saved_place/store/{id}','GuestSavePlaceController@store')->name('UserSavedPlace');
    Route::post('/saved_place/delete/{id}','GuestSavePlaceController@delete')->name('UserDeleteSavedPlace');

    // route for save place in all place all category
    Route::post('/saved_place/store/{id}','GuestSavePlaceController@store')->name('UserSavedPlace');

    //route for save place in all place each category
    Route::post('/{category_id}/list/saved_place/store/{id}','GuestSavePlaceController@store')->name('UserEachCategorySavedPlace');

});


    // route for tourist user
    Route::get('/myguest','User\GuestController@index')->name('UserHome');
    Route::get('/myguest/places','User\GuestController@all_category_place')->name('UserAllCategoryPlace');
    Route::get('/myguest/saved_place','User\GuestSavePlaceController@index')->name('UserShowSavePlace');
    Route::get('/myguest/plan','User\GuestController@all_category_place')->name('UserShowPlan');
    Route::get('/myguest/{id}/list','User\GuestController@list_all_places')->name('UserlistAllPlacesInCategory');

// testing route
Route::view('/create2','admin.place.create2');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
