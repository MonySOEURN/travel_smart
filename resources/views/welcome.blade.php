@extends('layouts.user.app', ['navigations'=> $navigations, 'categories'=> $categories])

@section('layout_content')
    <section class="ftco-section services-section bg-light">
        <div class="container">
            <div class="row d-flex">
                @foreach ($introductions as $introduction)
                <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services d-block text-center">
                        <div class="d-flex justify-content-center"><div class="icon"><span class={{$introduction->introduction_icon}}></span></div></div>
                        <div class="media-body p-2 mt-2">
                            <h3 class="heading mb-3">{{$introduction->introduction_name}}</h3>
                            <p>{{$introduction->introduction_detail}}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        </section>

        {{-- <section class="ftco-section ftco-destination">
            <div class="container">
                <div class="row justify-content-start mb-5 pb-3">
            <div class="col-md-7 heading-section ftco-animate">
                <span class="subheading">Featured</span>
                <h2 class="mb-4"><strong>Featured</strong> Destination</h2>
            </div>
            </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="destination-slider owl-carousel ftco-animate">
                                @foreach ($features as $feature)
                                    <div class="item">
                                        <div class="destination">
                                            <a href="#" class="img d-flex justify-content-center align-items-center" style="background-image: url('{{$feature->feature_photo}}');">
                                                <div class="icon d-flex justify-content-center align-items-center">
                                                    <span class="icon-search2"></span>
                                                </div>
                                            </a>
                                            <div class="text p-3">
                                                <h3><a href="#">{{$feature->feature_name}}</a></h3>
                                                <span class="listing">15 Listing</span>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section> --}}

        {{-- <section class="ftco-section bg-light">
            <div class="container">
                    <div class="row justify-content-start mb-5 pb-3">
            <div class="col-md-7 heading-section ftco-animate">
                <span class="subheading">Special Offers</span>
                <h2 class="mb-4"><strong>Top</strong> Rated Places</h2>
            </div>
            </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    @yield('top-rated')
                </div>
            </div>
        </section> --}}


        <section class="ftco-section bg-light">
            <div class="container">
                    <div class="row justify-content-start mb-5 pb-3">
                <div class="col-md-7 heading-section ftco-animate">
                    <span class="subheading">Special Offers</span>
                <h2 class="mb-4"><strong>Morning</strong> Places</h2>
                </div>
            </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    @foreach ($morning_places as $morning_place)
                        <div class="col-md col-md-6 col-lg ftco-animate">
                            <div class="destination">
                                <a href="#" class="img img-2 d-flex justify-content-center align-items-center" style="background-image: url('/guest/images/destination-1.jpg');">
                                    <div class="icon d-flex justify-content-center align-items-center">
                                        <span class="icon-search2"></span>
                                    </div>
                                </a>
                                <div class="text p-3">
                                    <div class="d-flex">
                                        <div class="one">
                                            <h3><a href="#">{{$morning_place->place_name}}</a></h3>
                                            <p class="rate">
                                                <i class="icon-star"></i>
                                                <i class="icon-star"></i>
                                                <i class="icon-star"></i>
                                                <i class="icon-star"></i>
                                                <i class="icon-star-o"></i>
                                                <span>8 Rating</span>
                                            </p>
                                        </div>
                                        <div class="two">
                                            <span class="price">$10</span>
                                        </div>
                                    </div>
                                    <p>Far far away, behind the word mountains, far from the countries</p>
                                    <p class="days"><span>2 days 3 nights</span></p>
                                    <hr>
                                    <p class="bottom-area d-flex">
                                        <span><i class="icon-map-o"></i> Siem Reap, CAMBODIA</span>
                                        <span class="ml-auto"><a href="#">Discover</a></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <a href="{{route('UserlistAllPlacesInCategory',['id'=> 1])}}" style="float:right;"><span class="btn btn-info btn-rounded">See more <i class='fas fa-angle-double-right' style='font-size:16px'></i></span></a>
            </div>
        </section>


        <section class="ftco-section bg-light">
            <div class="container">
                    <div class="row justify-content-start mb-5 pb-3">
                <div class="col-md-7 heading-section ftco-animate">
                    <span class="subheading">Special Offers</span>
                <h2 class="mb-4"><strong>Evening</strong> Places</h2>
                </div>
            </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                     @foreach ($evening_places as $evening_place)
                        <div class="col-md col-md-6 col-lg ftco-animate">
                            <div class="destination">
                                <a href="#" class="img img-2 d-flex justify-content-center align-items-center" style="background-image: url('/guest/images/destination-1.jpg');">
                                    <div class="icon d-flex justify-content-center align-items-center">
                                        <span class="icon-search2"></span>
                                    </div>
                                </a>
                                <div class="text p-3">
                                    <div class="d-flex">
                                        <div class="one">
                                            <h3><a href="#">{{$evening_place->place_name}}</a></h3>
                                            <p class="rate">
                                                <i class="icon-star"></i>
                                                <i class="icon-star"></i>
                                                <i class="icon-star"></i>
                                                <i class="icon-star"></i>
                                                <i class="icon-star-o"></i>
                                                <span>8 Rating</span>
                                            </p>
                                        </div>
                                        <div class="two">
                                            <span class="price">$10</span>
                                        </div>
                                    </div>
                                    <p>Far far away, behind the word mountains, far from the countries</p>
                                    <p class="days"><span>2 days 3 nights</span></p>
                                    <hr>
                                    <p class="bottom-area d-flex">
                                        <span><i class="icon-map-o"></i> Siem Reap, CAMBODIA</span>
                                        <span class="ml-auto"><a href="#">Discover</a></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <a href="{{route('UserlistAllPlacesInCategory',['id'=> 2])}}" style="float:right; color: #f85959;"><span class="btn btn-info btn-rounded">See more <i class='fas fa-angle-double-right' style='font-size:16px'></i></span></a>
            </div>
        </section>


        <section class="ftco-section bg-light">
            <div class="container">
                    <div class="row justify-content-start mb-5 pb-3">
                <div class="col-md-7 heading-section ftco-animate">
                    <span class="subheading">Special Offers</span>
                <h2 class="mb-4"><strong>Night</strong> Places</h2>
                </div>
            </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    @foreach ($night_places as $night_place)
                        <div class="col-md col-md-6 col-lg ftco-animate">
                            <div class="destination">
                                <a href="#" class="img img-2 d-flex justify-content-center align-items-center" style="background-image: url('/guest/images/destination-1.jpg');">
                                    <div class="icon d-flex justify-content-center align-items-center">
                                        <span class="icon-search2"></span>
                                    </div>
                                </a>
                                <div class="text p-3">
                                    <div class="d-flex">
                                        <div class="one">
                                            <h3><a href="#">{{$night_place->place_name}}</a></h3>
                                            <p class="rate">
                                                <i class="icon-star"></i>
                                                <i class="icon-star"></i>
                                                <i class="icon-star"></i>
                                                <i class="icon-star"></i>
                                                <i class="icon-star-o"></i>
                                                <span>8 Rating</span>
                                            </p>
                                        </div>
                                        <div class="two">
                                            <span class="price">$10</span>
                                        </div>
                                    </div>
                                    <p>Far far away, behind the word mountains, far from the countries</p>
                                    <p class="days"><span>2 days 3 nights</span></p>
                                    <hr>
                                    <p class="bottom-area d-flex">
                                        <span><i class="icon-map-o"></i> Siem Reap, CAMBODIA</span>
                                        <span class="ml-auto"><a href="#">Discover</a></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <a href="{{route('UserlistAllPlacesInCategory',['id'=> 3])}}" style="float:right;"><span class="btn btn-info btn-rounded">See more <i class='fas fa-angle-double-right' style='font-size:16px'></i></span></a>
            </div>
        </section>

        <section class="ftco-section ftco-counter img" id="section-counter" style="background-image: url('/guest/images/bg_1.jpg');">
            <div class="container">
                <div class="row justify-content-center mb-5 pb-3">
            <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
                <h2 class="mb-4">Some fun facts</h2>
                <span class="subheading">More than 100,000 websites hosted</span>
            </div>
            </div>
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <div class="row">
                    <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                        <div class="block-18 text-center">
                        <div class="text">
                            <strong class="number" data-number="100000">0</strong>
                            <span>Happy Customers</span>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                        <div class="block-18 text-center">
                        <div class="text">
                            <strong class="number" data-number="40000">0</strong>
                            <span>Destination Places</span>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                        <div class="block-18 text-center">
                        <div class="text">
                            <strong class="number" data-number="87000">0</strong>
                            <span>Hotels</span>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                        <div class="block-18 text-center">
                        <div class="text">
                            <strong class="number" data-number="56400">0</strong>
                            <span>Restaurant</span>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            </div>
        </section>
@endsection

