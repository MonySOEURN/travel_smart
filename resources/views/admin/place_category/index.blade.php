@extends('layouts.admin.app')

@section('layout_content')
<h4 style="color:green;">{{ Session::get('message')}}</h4>
{{-- table table-striped w-auto --}}
    <h1>All Place Categories: </h1>

    <table class="table">
        <thead>
            <th>No</th>
            <th>Category name</th>
        </thead>
            @foreach ($categories as $category)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$category->category_name}}</td>
                </tr>
            @endforeach
        <tbody>

        </tbody>
    </table>

@endsection
