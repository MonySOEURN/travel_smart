@extends('layouts.admin.app')

@section('layout_content')
    <div>
        <H1>Fill place Information: </H1>
        {{-- enctype="multipart/form-data" --}}
        <form action="{{route('AdminStorePlace')}}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="col">
                        <div class="form-group">
                                <label for="placeName">Place Name: </label>
                                <input type="text" class="form-control" name="place_name" id="place_name" aria-describedby="placeName" placeholder="Place Name">
                        </div>
                        <div class="form-group">
                            <label for="spendTime">Spend Time: </label>
                            <input type="text" class="timepicker form-control" name="time_spend" id="spend_time" aria-describedby="spendTime" placeholder="Time user should spend">
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <label for="place_rate">Place Rate: </label>
                                    <input type="number" class="form-control" name="place_rate" placeholder="Rate place">
                                </div>
                                <div class="col">
                                    <label for="place_detail">Place Detail: </label>
                                    <input type="text" class="form-control" name="place_detail" placeholder="Decription of Place">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <label for="lat_titude">Latitude: </label>
                                    <input type="text" class="form-control" name="lat_titude" placeholder="Latitude location">
                                </div>
                                <div class="col">
                                    <label for="short_titude">Short Titude: </label>
                                    <input type="text" class="form-control" name="short_titude" placeholder="Short Titude location">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Category: </label>
                                        <select name="category_id" id="exampleFormControlSelect1" class="form-control" style="height: calc(2.125em + 2px);">
                                            <option value="null">Choose Category...</option>
                                            @foreach ($categories as $category)
                                                <option value="{{$category->category_id}}">{{$category->category_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col">
                                    <label for="exampleFormControlSelect1">Place Available Time: </label>
                                    <select name="place_available_time_id" id="exampleFormControlSelect1" class="form-control" style="height: calc(2.125em + 2px);">
                                        <option value="null">Choose Place Availablet Time...</option>
                                        @foreach ($place_available_times as $place_available_time)
                                            <option value="{{$place_available_time->place_available_time_id}}">{{$place_available_time->place_available_time_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Feature: </label>
                            <select name="feature_id" id="exampleFormControlSelect1" class="form-control" style="height: calc(2.125em + 2px);">
                                <option value="null">Choose Feature...</option>
                                @foreach ($features as $feature)
                                    <option value="{{$feature->feature_id}}">{{$feature->feature_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        {{-- ------------ --}}
                        <div class="form-group">
                            <label for="place_photo">Place Photo: </label>
                            <input type="file" class="form-control" name="place_photo" placeholder="address" enc>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary" style="float:center;">Submit</button>
                        </div>
                    </div>
                </div>

        </form>
    </div>
    <script type="text/javascript">

        $('.timepicker').datetimepicker({

            format: 'HH:mm:ss'

        });

    </script>
@endsection
