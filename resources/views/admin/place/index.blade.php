@extends('layouts.admin.app')

@section('layout_content')
<h4 style="color:green;">{{ Session::get('message')}}</h4>
{{-- table table-striped w-auto --}}
    <h1>Place List: </h1>
    <table class="table table-sm">
        <thead>
            <tr>
                <th>No</th>
                <th>Place Picture</th>
                <th>Place name</th>
                <th colspan="2">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($places as $place)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td><img src="{{ asset($place->place_photo)}}" alt="picture te" width="50px" height="50px"></td>
                    <td>{{$place->place_name}}</td>
                    <td><a  href="/pageManager/place/{{$place->place_id}}/edit" ><button><i class="fa fa-edit"></i> Edit</button></a></td>
			        <td><a onclick="return confirm('Are you sure to delete {{$place->place_name}}?')" href="/pageManager/place/{{$place->place_id}}/delete" style="color:red;"><button><i class="fas fa-trash-alt" ></i> Delete</button></a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div>
        {{$places->links()}}
    </div>
@endsection
