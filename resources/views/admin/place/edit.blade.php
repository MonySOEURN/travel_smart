@extends('layouts.admin.app')

@section('layout_content')

<script type="text/javascript">

    $(document).ready(function() {
    // all custom jQuery will go here
        function editTime(){
            $('.timepicker').datetimepicker({
                format: 'HH:mm:ss'
            });
        }
    });
</script>
    <div>
        <H1>Fill place Information: </H1>
        {{-- enctype="multipart/form-data" --}}
        <form action="{{route('AdminUpdatePlace',['id'=> $place->place_id])}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="col">
                        <div class="form-group">
                                <label for="placeName">Place Name: </label>
                        <input type="text" class="form-control" name="place_name" id="place_name" aria-describedby="placeName" placeholder="Place Name" value="{{$place->place_name}}">
                        </div>
                        <div class="form-group">
                            <label for="spendTime">Spend Time: </label>
                            <input type="text" class="timepicker form-control" name="time_spend" id="spend_time" aria-describedby="spendTime" placeholder="Time user should spend" value="{{$place->time_spend}}" onclick="editTime()">
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <label for="lat_titude">Latitude: </label>
                                <input type="text" class="form-control" name="lat_titude" placeholder="Latitude location" value="{{$place->lat_titude}}">
                                </div>
                                <div class="col">
                                    <label for="short_titude">Short Titude: </label>
                                <input type="text" class="form-control" name="short_titude" placeholder="Short Titude location" value="{{$place->short_titude}}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Category: </label>
                                        <select name="category_id" id="exampleFormControlSelect1" class="form-control" style="height: calc(2.125em + 2px);">
                                            <option value="{{$place->category->category_id}}">{{$place->category->category_name}}</option>
                                            @foreach ($categories as $category)
                                                <option value="{{$category->category_id}}">{{$category->category_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col">
                                    <label for="exampleFormControlSelect1">Place Available Time: </label>
                                    <select name="place_available_time_id" id="exampleFormControlSelect1" class="form-control" style="height: calc(2.125em + 2px);">
                                        <option value="{{$place->place_available_time->place_available_time_id}}">{{$place->place_available_time->place_available_time_name}}</option>
                                        @foreach ($place_available_times as $place_available_time)
                                            <option value="{{$place_available_time->place_available_time_id}}">{{$place_available_time->place_available_time_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Feature: </label>
                            <select name="feature_id" id="exampleFormControlSelect1" class="form-control" style="height: calc(2.125em + 2px);" >
                                <option value="{{$place->feature->feature_id}}">{{$place->feature->feature_name}}</option>
                                @foreach ($features as $feature)
                                    <option value="{{$feature->feature_id}}">{{$feature->feature_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        {{-- ------------ --}}
                        <div class="form-group">
                            <label for="place_photo">Place Photo: </label>
                        <input type="file" class="form-control" name="place_photo" placeholder="address" value="{{$place->place_photo}}">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary" style="float:center;">Submit</button>
                        </div>
                    </div>
                </div>

        </form>
    </div>
@endsection
