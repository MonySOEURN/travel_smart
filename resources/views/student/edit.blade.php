@extends('layouts.app')


@section('header')
    <h1>To Make Registration for Student</h1>
@endsection


@section('content')


<h1>Welcome to Edit</h1>

<div class="signup-form">
    <form action="{{route('UpdateStudent',['id'=>$student->id])}}" method="post">
        @csrf
        <h2>Updade Student Information</h2>
        <p class="hint-text">Student Information.</p>
        <div class="form-group">
            <div class="row">
                <div class="col-xs-6">
                    <input type="text" class="form-control" name="name" placeholder="First Name" required="required" value="{{$student->name}}">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
             <div class="col-xs-6">
              <input type="number" class="form-control" name="score" placeholder="Score" required="required" value="{{$student->score}}">
             </div>
            </div>
        </div>
        <div class="form-group">
            <label class="checkbox-inline">
                <input type="checkbox" required="required"> I accept the <a href="#">Terms of Use</a> &amp; <a href="#">Privacy Policy</a></label>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-success btn-lg btn-block">Save Edit</button>
        </div>
    </form>
</div>

@endsection
