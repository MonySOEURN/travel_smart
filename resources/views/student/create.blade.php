@extends('layouts.app')


@section('header')
    <h1>To Make Registration for Student</h1>
@endsection

@section('content')


<div class="signup-form">
    <form action="{{route('NewStudentInfo')}}" method="post">
        @csrf
        <h2>Register</h2>
        <p class="hint-text">Student Registration Form.</p>
        <div class="form-group">
            <div class="row">
                <div class="col-xs-6">
                    <input type="text" class="form-control" name="name" placeholder="First Name" required="required">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
             <div class="col-xs-6">
              <input type="number" class="form-control" name="score" placeholder="Score" required="required">
             </div>
            </div>
        </div>
        <div class="form-group">
            <label class="checkbox-inline">
                <input type="checkbox" required="required"> I accept the <a href="#">Terms of Use</a> &amp; <a href="#">Privacy Policy</a></label>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-success btn-lg btn-block">Register Now</button>
        </div>
    </form>
</div>


@endsection
