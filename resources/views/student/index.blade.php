@extends('layouts.app')

@section('button')
	<a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="{{route('CreateStudent')}}">Register More Student</a>
@endsection

@section('content')

    <h1>All Student in the List: </h1>
	
	<table class="table table-bordered">
		<button></button>
	  <thead>
	    <tr>
    		<th>No</th>
    		<th>Name</th>
    		<th>Score</th>
    		<th colspan="2">Modify able</th>
	    </tr>
	  </thead>
	  <tbody>
	    @foreach($students as $student)
	    <tr>
			<td>{{$loop->iteration}}</td>
			<td>{{$student->name}}</td>
			<td>{{$student->score}}</td>
			<td><a  href="/students/{{$student->id}}/edit" ><button><i class="fas fa-pen" ></i> Edit</button></a></td>
			<td><a onclick="return confirm('Are you sure to delete {{$student->name}}?')" href="/students/{{$student->id}}/delete" ><button><i class="fas fa-trash-alt" ></i> Delete</button></a></td>
	    </tr>
	    @endforeach
	  </tbody>
	</table>

	
@endsection
