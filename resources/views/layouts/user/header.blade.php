<head>
    <title>Home - Travel Smart</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Alex+Brush" rel="stylesheet">
	<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>

    <link rel="stylesheet" href="{{ asset('guest/css/open-iconic-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('guest/css/animate.css')}}">

    <link rel="stylesheet" href="{{ asset('guest/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('guest/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{ asset('guest/css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{ asset('guest/css/aos.css')}}">

    <link rel="stylesheet" href="{{ asset('guest/css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{ asset('guest/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{ asset('guest/css/jquery.timepicker.css')}}">
    {{-- <base href="http://www.cs.tut.fi/~jkorpela/basic.css"> --}}

    <link rel="stylesheet" href="{{ asset('guest/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{ asset('guest/css/icomoon.css')}}">
    <link rel="stylesheet" href="{{ asset('guest/css/style.css')}}">

    {{-- jquery --}}
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
</head>
