<script src="{{ asset('/guest/js/jquery.min.js')}}"></script>
<script src="{{ asset('guest/js/jquery-migrate-3.0.1.min.js')}}"></script>
<script src="{{ asset('guest/js/popper.min.js')}}"></script>
<script src="{{ asset('guest/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('guest/js/jquery.easing.1.3.js')}}"></script>
<script src="{{ asset('guest/js/jquery.waypoints.min.js')}}"></script>
<script src="{{ asset('guest/js/jquery.stellar.min.js')}}"></script>
<script src="{{ asset('guest/js/owl.carousel.min.js')}}"></script>
<script src="{{ asset('guest/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{ asset('guest/js/aos.js')}}"></script>
<script src="{{ asset('guest/js/jquery.animateNumber.min.js')}}"></script>
<script src="{{ asset('guest/js/bootstrap-datepicker.js')}}"></script>
{{-- <script src="{{ asset('guest/js/jquery.timepicker.min.js')}}"></script> --}}
<script src="{{ asset('guest/js/scrollax.min.js')}}"></script>
{{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script> --}}
<script src="{{ asset('guest/js/google-map.js')}}"></script>
<script src="{{ asset('guest/js/main.js')}}"></script>

{{-- bootstrap js --}}
{{-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> --}}
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> --}}


<script>

$(document).ready(function(){
    var id = null;
    $('#MyModal').on('show.bs.modal', function(event){

    var button = $(event.relatedTarget);
    var place_id = button.data('index');
    var place_name = button.data('place_name');
    var place_photo = button.data('place_photo');
    var place_detail = button.data('place_detail');

    var modal = $(this);
    id = place_id;

    modal.find(".modal-body .save_place .place_id").val(place_id);
    modal.find(".modal-header .modal-title").text(place_name);
    modal.find(".modal-body img").attr('src',place_photo);
    modal.find(".modal-body .place_detail").text(place_detail);
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function isEven(number){
        if( number % 2 === 0 ){
            return true;
        } else {
            return false;
        }
    }

var count = 0;
    $('#save_place').on( 'click', function(event){
        var button = $(this);
        count++;
        var myStoreUrl = 'myguest/saved_place/store/'+id;
        var myDeleteUrl = 'myguest/saved_place/delete/'+id;
        if(isEven(count)){
            console.log('Even');

            event.preventDefault();
            $.ajax({
                type: "POST",
                url: myDeleteUrl,
                data: $('form#save_place').serialize(),
                success: function(response) {
                        button.css('background-color','#17a2b8');
                        button.css('border-color','#17a2b8');
                        console.log(response)
                },
                error: function(err) {
                    console.log(err);
                    alert('Error');
                }
            });
        } else {
            console.log('Odd');
            // console.log(myUrl);
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: myStoreUrl,//'myguest/saved_place/store/'+id,
                data: $('form#save_place').serialize(),
                success: function(response) {
                    if(response === 'true'){
                        button.css('background-color','red');
                        button.css('border-color','red');
                        console.log(response);
                    }else {
                        window.location = "/login";
                    }
                    // console.log(response)
                },
                error: function(err) {
                    console.log(err);
                    alert('Error');
                }
            });
        }
        // console.log('Button is clicked');
        // console.log(id);
    return false;

    });
});
</script>
