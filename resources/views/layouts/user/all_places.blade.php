@extends('layouts.app')
@section('all_places')
    
    <section class="ftco-section services-section bg-light">
            <div class="container">
              <div class="row d-flex">
                  @yield('introductions')
              </div>
            </div>
          </section>
          
          <section class="ftco-section ftco-destination">
              <div class="container">
                  <div class="row justify-content-start mb-5 pb-3">
                <div class="col-md-7 heading-section ftco-animate">
                    <span class="subheading">Featured</span>
                  <h2 class="mb-4"><strong>Featured</strong> Destination</h2>
                </div>
              </div>
                  <div class="row">
                      <div class="col-md-12">
                          <div class="destination-slider owl-carousel ftco-animate">
                              @yield('features')
                          </div>
                      </div>
                  </div>
              </div>
          </section>
      
          <section class="ftco-section bg-light">
              <div class="container">
                      <div class="row justify-content-start mb-5 pb-3">
                <div class="col-md-7 heading-section ftco-animate">
                    <span class="subheading">Special Offers</span>
                  <h2 class="mb-4"><strong>Top</strong> Rated Places</h2>
                </div>
              </div>    		
              </div>
              <div class="container-fluid">
                  <div class="row">
                      @yield('top-rated')
                  </div> 
              </div>
          </section>
      
      
          <section class="ftco-section bg-light">
              <div class="container">
                      <div class="row justify-content-start mb-5 pb-3">
                  <div class="col-md-7 heading-section ftco-animate">
                      <span class="subheading">Special Offers</span>
                  <h2 class="mb-4"><strong>Morning</strong> Places</h2>
                  </div>
              </div>    		
              </div>
              <div class="container-fluid">
                  <div class="row">
                      @yield('morning-places')
                  </div> 
                  <a href="{{route('')}}" class="btn btn-info btn-rounded" style="float:right;"><span class="btn btn-info btn-rounded">See more <i class='fas fa-angle-double-right' style='font-size:16px'></i></span></a>
              </div>
          </section>
      
              
          <section class="ftco-section bg-light">
              <div class="container">
                      <div class="row justify-content-start mb-5 pb-3">
                  <div class="col-md-7 heading-section ftco-animate">
                      <span class="subheading">Special Offers</span>
                  <h2 class="mb-4"><strong>Evening</strong> Places</h2>
                  </div>
              </div>    		
              </div>
              <div class="container-fluid">
                  <div class="row">
                      @yield('evening-places')
                  </div> 
                  <a href="contact.html" class="btn btn-info btn-rounded" style="float:right; color: #f85959;"><span class="btn btn-info btn-rounded">See more <i class='fas fa-angle-double-right' style='font-size:16px'></i></span></a>
              </div>
          </section>
      
              
          <section class="ftco-section bg-light">
              <div class="container">
                      <div class="row justify-content-start mb-5 pb-3">
                  <div class="col-md-7 heading-section ftco-animate">
                      <span class="subheading">Special Offers</span>
                  <h2 class="mb-4"><strong>Night</strong> Places</h2>
                  </div>
              </div>    		
              </div>
              <div class="container-fluid">
                  <div class="row">
                      @yield('night-places')
                  </div> 
                  <a href="contact.html" class="btn btn-info btn-rounded" style="float:right;"><span class="btn btn-info btn-rounded">See more <i class='fas fa-angle-double-right' style='font-size:16px'></i></span></a>
              </div>
          </section>
      
          <section class="ftco-section ftco-counter img" id="section-counter" style="background-image: url('/guest/images/bg_1.jpg');">
              <div class="container">
                  <div class="row justify-content-center mb-5 pb-3">
                <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
                  <h2 class="mb-4">Some fun facts</h2>
                  <span class="subheading">More than 100,000 websites hosted</span>
                </div>
              </div>
                  <div class="row justify-content-center">
                      <div class="col-md-10">
                          <div class="row">
                        <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                          <div class="block-18 text-center">
                            <div class="text">
                              <strong class="number" data-number="100000">0</strong>
                              <span>Happy Customers</span>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                          <div class="block-18 text-center">
                            <div class="text">
                              <strong class="number" data-number="40000">0</strong>
                              <span>Destination Places</span>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                          <div class="block-18 text-center">
                            <div class="text">
                              <strong class="number" data-number="87000">0</strong>
                              <span>Hotels</span>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                          <div class="block-18 text-center">
                            <div class="text">
                              <strong class="number" data-number="56400">0</strong>
                              <span>Restaurant</span>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
              </div>
              </div>
          </section>
          
@endsection