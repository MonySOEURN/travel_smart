@extends('layouts.user.app', ['navigations'=> $navigations, 'categories'=> $categories])

@section('layout_content')

    <section class="ftco-section bg-light">
        <div class="container">
                <div class="row justify-content-start mb-5 pb-3">
            <div class="col-md-7 heading-section ftco-animate">
                <span class="subheading">Special Offers</span>
            <h2 class="mb-4"><strong>All Places</strong> Places</h2>
            </div>
        </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                @foreach ($places as $place)
                    <div class="col-3 ftco-animate">
                        <div class="destination" style="cursor:pointer;" id="MyPlace" data-index={{$place->place_id}} data-place_name="{{$place->place_name}}" data-place_photo="{{$place->place_photo}}" data-place_detail="{{$place->place_detail}}" data-toggle="modal" data-target="#MyModal">
                            <a class="img img-2 d-flex justify-content-center align-items-center" style="background-image: url({{$place->place_photo}});">
                                <div class="icon d-flex justify-content-center align-items-center">
                                    <span class="icon-search2"></span>
                                </div>
                            </a>
                            <div class="text p-3">
                                <div class="d-flex">
                                    <div class="one">
                                        <h3><a >{{$place->place_name}}</a></h3>
                                        <p class="rate">
                                        @for ($i = 0; $i < 5; $i++)
                                            @if($i < $place->place_rate)
                                            <i class="icon-star"></i>
                                            @else
                                            <i class="icon-star-o"></i>
                                            @endif
                                        @endfor
                                            {{-- <span>8 Rating</span> --}}
                                        </p>
                                    </div>
                                    <div class="two">
                                        {{-- <span class="price">$10</span> --}}
                                    </div>
                                </div>
                            <p style="overflow: hidden;text-overflow: ellipsis;width: 252.75px; height: 50px;">{{$place->place_detail}}</p>
                                {{-- <p class="days"><span>2 days 3 nights</span></p> --}}
                                <hr>
                                <p class="bottom-area d-flex">
                                    <span><i class="icon-map-o"></i> Siem Reap, CAMBODIA</span>
                                    <span class="ml-auto"><a >Discover</a></span>
                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div>
                {{ $places->links() }}
            </div>
        </div>
        <div id="MyModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <h4 style="color:green;">{{ Session::get('message')}}</h4>
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Modal Title</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body ">
                            <div>
                                <img src="{{$place->place_photo}}" alt="place photo" width="100%" height="500px;">
                            </div>
                            <div style="display: inline-block;  width: 80%; vertical-align: top; margin-top: 20px">
                                <p class="place_time">Place Time</p>
                                <p class="place_rate">{{$place->place_rate}}</p>
                            </div>
                            <div style="display: inline-block; vertical-align: top;">
                                <form class="save_place" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="place_id" class="place_id" value="testing">
                                    <button id="save_place" class="btn btn-info" style="float:right; margin-top: 20px; ">Save Place</button>
                                </form>
                            </div>
                            <hr>
                            <div>
                                <p class="place_detail">Detail of place Here</p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

<script>

        $(document).ready(function(){
            var id = null;
            $('#MyModal').on('show.bs.modal', function(event){

            // console.log("Modal Opened");
            var button = $(event.relatedTarget);
            // console.log(button);
            var place_id = button.data('index');
            var place_name = button.data('place_name');
            var place_photo = button.data('place_photo');
            var place_detail = button.data('place_detail');
            // var place_available_time = button.data('place_available_time');
            // console.log(place_id);
            // console.log(place_name);
            // console.log(place_photo);
            // console.log(place_detail);

            var modal = $(this);
            id = place_id;
            // console.log(place_available_time);
            // console.log(modal.find(".modal-body .save_place .place_id").val())
            modal.find(".modal-body .save_place .place_id").val(place_id);
            // id = modal.find(".modal-body .save_place .place_id").val();
            // console.log(modal.find(".modal-body .save_place .place_id").text())
            modal.find(".modal-header .modal-title").text(place_name);
            // console.log(modal.find(".modal-header h5").text())
            modal.find(".modal-body img").attr('src',place_photo);
            // console.log(modal.find(".modal-body img").text())
            modal.find(".modal-body .place_detail").text(place_detail);
            // console.log(modal.find(".modal-body p").text())

            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            function isEven(number){
                if( number % 2 === 0 ){
                    return true;
                } else {
                    return false;
                }
            }

        var count = 0;
            $('#save_place').on( 'click', function(event){
                var button = $(this);
                count++;
                var myStoreUrl = 'myguest//saved_place/store/'+id;
                var myDeleteUrl = 'myguest/saved_place/delete/'+id;
                if(isEven(count)){
                    console.log('Even');

                    event.preventDefault();
                    $.ajax({
                        type: "POST",
                        url: myDeleteUrl,
                        data: $('form#save_place').serialize(),
                        success: function(response) {
                                button.css('background-color','#17a2b8');
                                button.css('border-color','#17a2b8');
                                console.log(response)
                        },
                        error: function(err) {
                            console.log(err);
                            alert('Error');
                        }
                    });
                } else {
                    console.log('Odd');
                    // console.log(myUrl);
                    event.preventDefault();
                    $.ajax({
                        type: "POST",
                        url: myStoreUrl,//'myguest/saved_place/store/'+id,
                        data: $('form#save_place').serialize(),
                        success: function(response) {
                            if(response === 'true'){
                                button.css('background-color','red');
                                button.css('border-color','red');
                                console.log(response);
                            }else {
                                window.location = "/login";
                            }
                            // console.log(response)
                        },
                        error: function(err) {
                            console.log(err);
                            alert('Error');
                        }
                    });
                }
                // console.log('Button is clicked');
                // console.log(id);
            return false;

            });
        });
        </script>
    </section>

@endsection
