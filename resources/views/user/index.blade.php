@extends('layouts.user.app', ['navigations'=> $navigations, 'categories'=> $categories])

@section('layout_content')
    <section class="ftco-section services-section bg-light">
        <div class="container">
            <div class="row d-flex">
                @foreach ($introductions as $introduction)
                <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services d-block text-center">
                        <div class="d-flex justify-content-center"><div class="icon"><span class={{$introduction->introduction_icon}}></span></div></div>
                        <div class="media-body p-2 mt-2">
                            <h3 class="heading mb-3">{{$introduction->introduction_name}}</h3>
                            <p>{{$introduction->introduction_detail}}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>


        <section class="ftco-section bg-light">
            <div class="container">
                    <div class="row justify-content-start mb-5 pb-3">
                <div class="col-md-7 heading-section ftco-animate">
                    <span class="subheading">Special Offers</span>
                <h2 class="mb-4"><strong>Morning</strong> Places</h2>
                </div>
            </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    @foreach ($morning_places as $morning_place)
                    <div class="col-md col-md-6 col-lg ftco-animate" style="cursor:pointer;">
                            <div class="destination" id="MyPlace" data-index={{$morning_place->place_id}} data-place_name="{{$morning_place->place_name}}" data-place_photo="{{$morning_place->place_photo}}" data-place_detail="{{$morning_place->place_detail}}" data-toggle="modal" data-target="#MyModal">
                                <a class="img img-2 d-flex justify-content-center align-items-center" style="background-image: url({{$morning_place->place_photo}});">
                                    <div class="icon d-flex justify-content-center align-items-center">
                                        <span class="icon-search2"></span>
                                    </div>
                                </a>
                                <div class="text p-3">
                                    <div class="d-flex">
                                        <div class="one">
                                            <h3><a class="testing" >{{$morning_place->place_name}}</a></h3>
                                            <p class="rate">
                                                @for ($i = 0; $i < 5; $i++)
                                                    @if($i < $morning_place->place_rate)
                                                    <i class="icon-star"></i>
                                                    @else
                                                    <i class="icon-star-o"></i>
                                                    @endif
                                                @endfor
                                                    {{-- <span>8 Rating</span> --}}
                                            </p>
                                        </div>
                                        <div class="two">
                                            <span class="price">$10</span>
                                        </div>
                                    </div>
                                    <p style="overflow: hidden;text-overflow: ellipsis;width: 252.75px; height: 50px;">{{$morning_place->place_detail}}</p>
                                    {{-- <p class="days"><span>2 days 3 nights</span></p> --}}
                                    <hr>
                                    <p class="bottom-area d-flex">
                                        <span><i class="icon-map-o"></i> Siem Reap, CAMBODIA</span>
                                        <span class="ml-auto"><a href="">Discover</a></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <a href="{{route('UserlistAllPlacesInCategory',['id'=> 1])}}" style="float:right;"><span class="btn btn-info btn-rounded">See more <i class='fas fa-angle-double-right' style='font-size:16px'></i></span></a>
            </div>
        </section>

        <section class="ftco-section bg-light">
                <div class="container">
                        <div class="row justify-content-start mb-5 pb-3">
                    <div class="col-md-7 heading-section ftco-animate">
                        <span class="subheading">Special Offers</span>
                    <h2 class="mb-4"><strong>Evening</strong> Places</h2>
                    </div>
                </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        @foreach ($evening_places as $evening_place)
                        <div class="col-md col-md-6 col-lg ftco-animate" style="cursor:pointer;">
                                <div class="destination" id="MyPlace" data-index={{$evening_place->place_id}} data-place_name="{{$evening_place->place_name}}" data-place_photo="{{$evening_place->place_photo}}" data-place_detail="{{$evening_place->place_detail}}" data-toggle="modal" data-target="#MyModal">
                                    <a class="img img-2 d-flex justify-content-center align-items-center" style="background-image: url({{$evening_place->place_photo}});">
                                        <div class="icon d-flex justify-content-center align-items-center">
                                            <span class="icon-search2"></span>
                                        </div>
                                    </a>
                                    <div class="text p-3">
                                        <div class="d-flex">
                                            <div class="one">
                                                <h3><a class="testing" >{{$evening_place->place_name}}</a></h3>
                                                <p class="rate">
                                                    @for ($i = 0; $i < 5; $i++)
                                                        @if($i < $evening_place->place_rate)
                                                        <i class="icon-star"></i>
                                                        @else
                                                        <i class="icon-star-o"></i>
                                                        @endif
                                                    @endfor
                                                        {{-- <span>8 Rating</span> --}}
                                                </p>
                                            </div>
                                            <div class="two">
                                                <span class="price">$10</span>
                                            </div>
                                        </div>
                                        <p style="overflow: hidden;text-overflow: ellipsis;width: 252.75px; height: 50px;">{{$evening_place->place_detail}}</p>
                                        {{-- <p class="days"><span>2 days 3 nights</span></p> --}}
                                        <hr>
                                        <p class="bottom-area d-flex">
                                            <span><i class="icon-map-o"></i> Siem Reap, CAMBODIA</span>
                                            <span class="ml-auto"><a href="">Discover</a></span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <a href="{{route('UserlistAllPlacesInCategory',['id'=> 2])}}" style="float:right;"><span class="btn btn-info btn-rounded">See more <i class='fas fa-angle-double-right' style='font-size:16px'></i></span></a>
                </div>
            </section>

            <section class="ftco-section bg-light">
                    <div class="container">
                            <div class="row justify-content-start mb-5 pb-3">
                        <div class="col-md-7 heading-section ftco-animate">
                            <span class="subheading">Special Offers</span>
                        <h2 class="mb-4"><strong>Night</strong> Places</h2>
                        </div>
                    </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            @foreach ($night_places as $night_place)
                            <div class="col-md col-md-6 col-lg ftco-animate" style="cursor:pointer;">
                                    <div class="destination" id="MyPlace" data-index={{$night_place->place_id}} data-place_name="{{$night_place->place_name}}" data-place_photo="{{$night_place->place_photo}}" data-place_detail="{{$night_place->place_detail}}" data-toggle="modal" data-target="#MyModal">
                                        <a class="img img-2 d-flex justify-content-center align-items-center" style="background-image: url({{$night_place->place_photo}});">
                                            <div class="icon d-flex justify-content-center align-items-center">
                                                <span class="icon-search2"></span>
                                            </div>
                                        </a>
                                        <div class="text p-3">
                                            <div class="d-flex">
                                                <div class="one">
                                                    <h3><a class="testing" >{{$night_place->place_name}}</a></h3>
                                                    <p class="rate">
                                                        @for ($i = 0; $i < 5; $i++)
                                                            @if($i < $night_place->place_rate)
                                                            <i class="icon-star"></i>
                                                            @else
                                                            <i class="icon-star-o"></i>
                                                            @endif
                                                        @endfor
                                                            {{-- <span>8 Rating</span> --}}
                                                    </p>
                                                </div>
                                                <div class="two">
                                                    <span class="price">$10</span>
                                                </div>
                                            </div>
                                            <p style="overflow: hidden;text-overflow: ellipsis;width: 252.75px; height: 50px;">{{$night_place->place_detail}}</p>
                                            {{-- <p class="days"><span>2 days 3 nights</span></p> --}}
                                            <hr>
                                            <p class="bottom-area d-flex">
                                                <span><i class="icon-map-o"></i> Siem Reap, CAMBODIA</span>
                                                <span class="ml-auto"><a href="">Discover</a></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <a href="{{route('UserlistAllPlacesInCategory',['id'=> 3])}}" style="float:right;"><span class="btn btn-info btn-rounded">See more <i class='fas fa-angle-double-right' style='font-size:16px'></i></span></a>
                    </div>
                </section>


        <section class="ftco-section ftco-counter img" id="section-counter" style="background-image: url('/guest/images/bg_1.jpg');">
            <div class="container">
                <div class="row justify-content-center mb-5 pb-3">
                    <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
                        <h2 class="mb-4">Some fun facts</h2>
                        <span class="subheading">More than 100,000 websites hosted</span>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <div class="row">
                        <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                            <div class="block-18 text-center">
                                <div class="text">
                                    <strong class="number" data-number="100000">0</strong>
                                    <span>Happy Customers</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                            <div class="block-18 text-center">
                                <div class="text">
                                    <strong class="number" data-number="40000">0</strong>
                                    <span>Destination Places</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                            <div class="block-18 text-center">
                                <div class="text">
                                    <strong class="number" data-number="87000">0</strong>
                                    <span>Hotels</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                            <div class="block-18 text-center">
                                <div class="text">
                                    <strong class="number" data-number="56400">0</strong>
                                    <span>Restaurant</span>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <!-- Modal -->
        </section>

        <div id="MyModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <h4 style="color:green;">{{ Session::get('message')}}</h4>
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Modal Title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body ">
                        <div>
                            <img src="{{$morning_place->place_photo}}" alt="place photo" width="100%" height="500px;">
                        </div>
                        <div style="display: inline-block;  width: 80%; vertical-align: top; margin-top: 20px">
                            <p class="place_time">Place Time</p>
                            <p class="place_rate">{{$morning_place->place_rate}}</p>
                        </div>
                        <div style="display: inline-block; vertical-align: top;">
                            <form class="save_place" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="place_id" class="place_id" value="testing">
                                <button id="save_place" class="btn btn-info" style="float:right; margin-top: 20px; ">Save Place</button>
                            </form>
                        </div>
                        <hr>
                        <div>
                            <p class="place_detail">Detail of place Here</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>


@endsection

