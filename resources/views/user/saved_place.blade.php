@extends('layouts.user.app', ['navigations'=> $navigations, 'categories'=> $categories])

@section('layout_content')
<section class="ftco-section bg-light">
    <div class="container">
            <div class="row justify-content-start mb-5 pb-3">
        <div class="col-md-7 heading-section ftco-animate">
            <span class="subheading">Special Offers</span>
        <h2 class="mb-4"><strong>Saved</strong> Places</h2>
        </div>
    </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            @if($saved_places->isEmpty())
                <strong style="font-size:20px; ">Nothing to show</strong>
            @endif
            @foreach ($saved_places as $saved_place)
                <div class="col-md col-md-6 col-lg ftco-animate">
                    <div class="destination">
                        <a class="img img-2 d-flex justify-content-center align-items-center" style="background-image: url({{$saved_place->place->place_photo}});">
                            <div class="icon d-flex justify-content-center align-items-center">
                                <span class="icon-search2"></span>
                            </div>
                        </a>
                        <div class="text p-3">
                            <div class="d-flex">
                                <div class="one">
                                    <h3><a >{{$saved_place->place->place_name}}</a></h3>
                                    <p class="rate">
                                        @for ($i = 0; $i < 5; $i++)
                                            @if($i < $saved_place->place->place_rate)
                                            <i class="icon-star"></i>
                                            @else
                                            <i class="icon-star-o"></i>
                                            @endif
                                        @endfor
                                    </p>
                                </div>
                                <div class="two">
                                    {{-- <span class="price">$10</span> --}}
                                </div>
                            </div>
                            <p style="overflow: hidden;text-overflow: ellipsis;width: 252.75px; height: 50px;">{{$saved_place->place->place_detail}}</p>
                            <hr>
                            <p class="bottom-area d-flex">
                                <span><i class="icon-map-o"></i> Siem Reap, CAMBODIA</span>
                                <span class="ml-auto"><a>Discover</a></span>
                            </p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div>
            {{-- {{ $saved_places->links() }} --}}
        </div>
    </div>
</section>

@endsection
