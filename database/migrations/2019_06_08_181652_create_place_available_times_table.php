<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlaceAvailableTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('place_available_times', function (Blueprint $table) {
            $table->bigIncrements('place_available_time_id');
            $table->string('place_available_time_name');
            $table->bigInteger('season_id')->unsigned();
            $table->foreign('season_id')->references('season_id')->on('seasons');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('place_available_times');
    }
}
