<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSavedPlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saved_places', function (Blueprint $table) {
            $table->bigIncrements('saved_place_id');
            $table->time('place_selected_time');
            $table->bigInteger('plan_id')->unsigned();
            $table->foreign('plan_id')->references('plan_id')->on('plans');
            $table->bigInteger('place_id')->unsigned();
            $table->foreign('place_id')->references('place_id')->on('places');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saved_places');
    }
}
