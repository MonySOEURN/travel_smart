<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places', function (Blueprint $table) {
            $table->bigIncrements('place_id');
            $table->string('place_name');
            $table->time('time_spend');
            $table->string('lat_titude');
            $table->string('short_titude');
            $table->string('place_photo');
            $table->string('place_rate');
            $table->string('place_detail');
            $table->bigInteger('place_available_time_id')->unsigned();
            $table->foreign('place_available_time_id')->references('place_available_time_id')->on('place_available_times');
            $table->bigInteger('category_id')->unsigned();
            $table->foreign('category_id')->references('category_id')->on('categories');
            $table->bigInteger('feature_id')->unsigned();
            $table->foreign('feature_id')->references('feature_id')->on('features');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('places');
    }
}
