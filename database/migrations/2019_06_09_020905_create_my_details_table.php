<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMyDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('my_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('my_user_id')->unsigned();
            $table->bigInteger('detail');
            $table->foreign('my_user_id')->references('id')->on('my_users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('my_details');
    }
}
