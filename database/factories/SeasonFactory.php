<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\season;
use Faker\Generator as Faker;

$factory->define(season::class, function (Faker $faker) {
    return [
        'season_name' => $faker->name,
        'time_detail' => $faker->time(43524),
    ];
});
