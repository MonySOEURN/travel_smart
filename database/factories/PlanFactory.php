<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\plan;
use Faker\Generator as Faker;

$factory->define(plan::class, function (Faker $faker) {
    return [
        'plan_name' => $faker->name,
        'plan_photo' => $faker->text(20),
        'description' => $faker->text(200),
        'user_id' => rand(1,1),
    ];
});
