<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\introduction;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Faker\Provider\ru_RU\Text;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\introduction::class, function (Faker $faker) {
    return [
        'introduction_name' => $faker->name,
        'introdution_detail' => $faker->text(350),
    ];
});
