<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\place_available_time;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Faker\Provider\ru_RU\Text;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\place_available_time::class, function (Faker $faker) {
    return [
        'place_available_time_name' => $faker->name,
        'season_id' => rand(1,4),
    ];
});
