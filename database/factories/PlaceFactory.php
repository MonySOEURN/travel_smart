<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\place;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Faker\Provider\ru_RU\Text;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\place::class, function (Faker $faker) {
    return [
        'place_name' => $faker->name,
        'time_spend' => $faker->time(),
        'short_titude' => $faker->text(150),
        'lat_titude' => $faker->text(150),
        'place_available_time_id' => rand(2,5),
        'category_id' => rand(2,4),
    ];
});
