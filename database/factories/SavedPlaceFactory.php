<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\saved_place;
use Faker\Generator as Faker;

$factory->define(saved_place::class, function (Faker $faker) {
    return [
        'place_selected_time' => $faker->time(),
        'plan_id' => rand(3,7),
        'place_id' => rand(3,33),
    ];
});
