<?php

use Illuminate\Database\Seeder;
use App\place_available_time;

class PlaceAvailableTimeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory(App\place_available_time::class, 4)->create();
        $placeAvailableTimes = [
            ['place_available_time_name' => '5AM to 6PM', 'season_id'=>'1'],
            ['place_available_time_name' => '7.30AM to 5.30PM', 'season_id'=>'2'],
            ['place_available_time_name' => 'Open all through the day', 'season_id'=>'2'],
            ['place_available_time_name' => '9AM to 5PM', 'season_id'=>'1'],
            ['place_available_time_name' => '8.30AM to 6PM', 'season_id'=>'1'],
        ];

        place_available_time::insert($placeAvailableTimes);
    }
}
