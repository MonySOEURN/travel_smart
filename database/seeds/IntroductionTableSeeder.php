<?php

use App\introduction;
use Illuminate\Database\Seeder;

class IntroductionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         // $this->call(UsersTableSeeder::class);
        //  factory(App\introduction::class, 4)->create();
        $introductions = [
            ['introduction_name' => 'Selected Place', 'introduction_detail'=>'You can view list of places and you can select the place for view.', 'introduction_icon' => 'flaticon-support'],
            ['introduction_name' => 'Save a Place', 'introduction_detail'=>'Saved all the place that you want to go for this trip for your trip', 'introduction_icon' => 'flaticon-like'],
            ['introduction_name' => 'Create Plan', 'introduction_detail'=>'Have all places! Form these place into your plan for this trip', 'introduction_icon' => 'flaticon-hotel'],
            ['introduction_name' => 'Enjoy with Plan', 'introduction_detail'=>'You can view, modified some place and review which place you have finished.', 'introduction_icon' => 'flaticon-meeting-point'],

        ];

        introduction::insert($introductions); 
    }
}
