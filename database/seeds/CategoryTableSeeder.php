<?php

use \App\category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         // $this->call(UsersTableSeeder::class);
        //  factory(App\category::class, 4)->create();
        $categories = [
            ['category_name' => 'Morning'],
            ['category_name' => 'Evening'],
            ['category_name' => 'Night'],
        ];

        category::insert($categories);   
    }
}
