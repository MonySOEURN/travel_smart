<?php

use Illuminate\Database\Seeder;

class SavedPlaceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\saved_place::class, 4)->create();
    }
}
