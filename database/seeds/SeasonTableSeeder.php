<?php

use App\season;
use Illuminate\Database\Seeder;

class SeasonTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         // $this->call(UsersTableSeeder::class);
        //  factory(App\season::class, 4)->create();
        $seasons = [
            ['season_name' => 'High', 'time_detail'=>'2019-05-18 06:10:00'],
            ['season_name' => 'Low', 'time_detail'=>'2019-09-18 08:10:00'],
        ];

        season::insert($seasons);
    }
}
