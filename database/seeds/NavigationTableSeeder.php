<?php

use Illuminate\Database\Seeder;
use App\navigation;

class NavigationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $navigations = [
            ['navigation_name' => 'Home', 'navigation_url'=>'UserHome', 'navigation_free1'=>'active'],
            ['navigation_name' => 'Places', 'navigation_url'=>'UserAllCategoryPlace', 'navigation_free1'=>''],
            ['navigation_name' => 'Saved Place', 'navigation_url'=>'UserShowSavePlace', 'navigation_free1'=>''],
            ['navigation_name' => 'Plan', 'navigation_url'=>'UserShowPlan', 'navigation_free1'=>''],
        ];

        navigation::insert($navigations);
    }
}
