<?php

use Illuminate\Database\Seeder;
use App\feature;

class FeatureTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $features = [
            ['feature_name' => 'Temple', 'feature_photo'=>'https://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Angkor_Wat.jpg/300px-Angkor_Wat.jpg', 'feature_description'=>'',],
            ['feature_name' => 'Water', 'feature_photo'=>'https://imgc.allpostersimages.com/img/print/u-g-P9CFBB0.jpg?w=550&h=550&p=0', 'feature_description'=>'',],
            ['feature_name' => 'Shopping', 'feature_photo'=>'http://static.asiawebdirect.com/m/bangkok/portals/cambodia/homepage/siem-reap/top10/10-best-siem-reap-shopping/pagePropertiesImage/10best-shopping.jpg', 'feature_description'=>'',],
            ['feature_name' => 'Mountain', 'feature_photo'=>'https://media2.trover.com/T/56ea6f307eb4f41dc807be21/fixedw_large_4x.jpg', 'feature_description'=>'',],
            ['feature_name' => 'Artificial Resort', 'feature_photo'=>'https://media-cdn.tripadvisor.com/media/photo-m/1280/17/4b/c0/94/eocambo-village.jpg', 'feature_description'=>'',],
        ];

        feature::insert($features); 
    }
}
