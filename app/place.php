<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class place extends Model
{
    protected $table = 'places';

    protected $fillable = [
        'place_name',
        'time_spend',
        'lat_titude',
        'short_titude',
        'place_photo',
        'place_rate',
        'place_detail',
        'place_available_time_id',
        'category_id',
        'feature_id'
    ];
    //
    protected $primaryKey = 'place_id';

    public function place_available_time(){
        return $this->belongsTo('App\place_available_time','place_available_time_id');
    }

    public function category(){
        return $this->belongsTo('App\category','category_id');
    }

    public function feature(){
        return $this->belongsTo('App\feature', 'feature_id');
    }

    // public function saved_place(){
    //     return $this->belongsTo('App\saved_place', 'saved_place_id');
    // }

}
