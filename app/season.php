<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class season extends Model
{

    protected $primaryKey = 'season_id';

    public function place_available_time(){
        return $this->hasMany('App\place_available_time','place_available_time_id');
    }
}
