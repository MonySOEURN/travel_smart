<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class saved_place extends Model
{
    //
    use SoftDeletes;
    protected $table = 'saved_places';
    protected $primaryKey = 'saved_place_id';

    protected $fillable = [
        'place_selected_time', 'plan_id', 'place_id', 'user_id'
    ];

    public function place(){
        return $this->belongsTo('App\place','place_id');
    }

    public function plan(){
        return $this->belongsTo('App\plan','plan_id');
    }

}
