<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
// use App\MyUser;

use App\category;
use App\place;
use App\saved_place;
use App\plan;
use App\user;
use App\season;
use App\place_available_time;

class TestController extends Controller
{
    public function index(){
        // echo 'Hello from testing Index.';
        $place_available_times = place_available_time::all();
        $places = place::all();
        $saved_places = saved_place::all();
        $plans = plan::all();
        $users = user::all();
        $seasons = season::all();
        $categorys = category::all();
        return view('user.testing',compact('place_available_times','places','saved_places','plans'
        ,'users','seasons','categorys'));
    }
}
