<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\category;

class CategoryController extends Controller
{
    //
    public function index(){
        $categories = category::all();
        return view("admin.place_category.index", compact('categories'));
    }
}
