<?php

namespace App\Http\Controllers\Admin;

use App\Place;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\category;
use App\feature;
use App\place_available_time;
use Illuminate\Support\Facades\DB;

class PlaceController extends Controller
{

    /**
     *
     */
    public function index(){
        $places = DB::table('places')->orderBy('place_id','desc')->paginate(10);
        return view('admin.place.index',compact('places'));
        //->with($places,'places');
    }

    /**
     * go to create place page
     */
    public function createPlace(){
        // return Place::find(1);
        $place_available_times = place_available_time::all();
        $categories = category::all();
        $features = feature::all();
        return view('admin.place.create', compact('place_available_times', 'categories', 'features'));
    }

    public function storePlace(Request $request){
        // dd($request);
        // $this->validate($request, [
        //     'place_name' => 'required',
        //     'time_spend' => 'required',
        //     'lat_titude' => 'required',
        //     'short_titude' => 'required',
        //     'place_photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        //     'place_available_time_id' => 'required',
        //     'category_id' => 'required',
        //     'feature_id' => 'required'
        // ]);

        $pictureInfo = $request->file('place_photo');

        $pictureName = $pictureInfo->getClientOriginalName();
        $pictureInfo->move('storage/',$pictureName);

        $pictureUrl = 'storage/'.$pictureName;

        Place::create([
            'place_name' => $request->place_name,
            'time_spend' => $request->time_spend,
            'lat_titude' => $request->lat_titude,
            'short_titude' => $request->short_titude,
            'place_photo' => $pictureUrl,
            'place_rate' => $request->place_rate,
            'place_detail' => $request->place_detail,
            'place_available_time_id' => $request->place_available_time_id,
            'category_id' => $request->category_id,
            'feature_id' => $request->feature_id
        ]);

        return redirect('admin/place')->with('message','place create success');
    }

    public function editPlace($id){
        // echo "editPlace";
        $place_available_times = place_available_time::all();
        $categories = category::all();
        $features = feature::all();
        $place = place::find($id);


        // echo $place->feature->feature_name;

        return view('admin.place.edit')
        ->with('place', $place)
        ->with('place_available_times', $place_available_times)
        ->with('features', $features)
        ->with('categories',$categories);

    }

    public function updatePlace(Request $request, $id){
        // return "Update Place";

        $pictureInfo = $request->file('place_photo');

        $pictureName = $pictureInfo->getClientOriginalName();
        $pictureInfo->move('storage/',$pictureName);

        // $pictureUrl = 'storage/'.$pictureName;

        $place = place::find($id);
        $place->place_name = $request->input('place_name');
        $place->time_spend = $request->input('time_spend');
        $place->lat_titude = $request->input('lat_titude');
        $place->short_titude = $request->input('short_titude');
        $place->place_photo = $pictureName;
        $place->place_available_time_id = $request->input('place_available_time_id');
        $place->category_id = $request->input('category_id');
        $place->feature_id = $request->input('feature_id');
        $place->save();

        return redirect('/pageManager/place');
    }

    public function deletePlace($id){
        $place = place::find($id);
        $place->delete();
        return redirect('/pageManager/place')->with('message','place delete success');;
    }


}
