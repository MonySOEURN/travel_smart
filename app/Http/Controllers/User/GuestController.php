<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\introduction;
use App\navigation;
use App\feature;
use App\category;
use App\place;
use Illuminate\Support\Facades\DB;

class GuestController extends Controller
{ 
    public function index(){
        $introductions = introduction::all();
        $navigations = navigation::all();
        $features = feature::all();
        $categories = category::all();

        $morning_places = DB::table('places')->where('category_id', '=', 1)->paginate(4);
        $evening_places = DB::table('places')->where('category_id', '=', 2)->paginate(4);
        $night_places = DB::table('places')->where('category_id', '=', 3)->paginate(4);
        return view('user.index',compact('introductions','navigations','features','categories','morning_places','evening_places','night_places'));
    }

    public function list_all_places($id){
        $navigations = navigation::all();
        $categories = category::all();
        $category_name = category::find($id);
        $list_places = DB::table('places')->where('category_id', '=', $id)->paginate(8);
        return view ('user.list_all_place', compact('navigations', 'categories', 'category_name', 'list_places'));
    }

    public function all_category_place(){
        $navigations = navigation::all();
        $categories = category::all();
        $places = DB::table('places')->paginate(12);
        return view('user.all_category_place', compact('navigations','categories','places'));
    }



}
