<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\saved_place;
use App\navigation;
use App\category;
use Illuminate\Support\Facades\DB;
use App\student;

// GuestSavePlaceController
class GuestSavePlaceController extends Controller
{
    //
    public function index(){
        if($user = Auth::user()){
            $userId = Auth::user()->id;
            $saved_places = saved_place::where('user_id', $userId)->get();//saved_place::all();
        } else {
            $saved_places = student::all();
        }
        $navigations = navigation::all();
        $categories = category::all();
        return view('user.saved_place', compact('navigations', 'categories', 'saved_places'));
    }

    /**
     * Store the select place for user
     * @param Request $request get request
     * @param $id get place id
     * @return string
     */
    public function store(Request $request, $id){
        // return "store";
        // console.log
        if(Auth::check()){
            // dd($request);
            saved_place::create([
                'place_selected_time' => '07:07:07',
                'plan_id' => '1',
                'place_id' => $id,
                'user_id' => auth()->user()->id,
            ]);

            return 'true';
            // return 'user saved place';
            // return redirect('/')->with('message','place create success');
        }
        else{
            return 'false';
            // return 'user is Not login';
            // return redirect('/login');
        }
        // return 'Nothing';

    }

    /**
     * Delete Saved place
     * @param $id place id
     * @return string
     */
    public function delete($id){
        return $id;
        saved_place::find($id)->delete();
        return 'place Delete';
    }

}
