<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// /**
//  * @SWG\Swagger(
//  *  basePath = "/api/v1"
//  *  @SWG\Info(
//  *   title = " Laravel Travel Smart Application APIs",
//  *   version = " 1.0.0",
//  *  )
//  * )
//  * Class BaseAPIController
//  * @package App\Http\Controllers\API
//  */

class BaseAPIController extends Controller
{
    public function __construct()
    {
        $this->middleware('api');
    }
}
