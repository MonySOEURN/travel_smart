<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\place;

class category extends Model
{
    //
    protected $primaryKey = 'category_id';

    // you have to specify the foriegn key if you not using foriegn key name "id"
    function place(){
        return $this->hasMany("App\place", 'category_id');
    }
}
