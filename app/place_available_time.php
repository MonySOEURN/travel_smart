<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class place_available_time extends Model
{
    
    protected $primaryKey = 'place_available_time_id';
    //
    public function season(){

        return $this->belongsTo('App\season','season_id');
    }

    public function place(){
        return $this->hasMany('App\place','place_id');
    }
}
